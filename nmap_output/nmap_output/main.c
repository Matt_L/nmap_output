//
//  main.c
//  nmap_output
//
//  Created by Matt Lavender on 10/06/12.
//  Copyright (c) 2012 BrownCow Apps. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <libxml/parser.h>

int main(int argc, const char * argv[])
{

	
	xmlDoc *document;
	xmlNode *root, *firstChild, *node;
	
		//Check if a path was entered.
	if (argc < 2) {
		printf("Usage: %s filename.xml\n", argv[0]);
		return 1;
		
	}
	
	char *filename = argv[1];
	
	printf("The file name is: %s\n\n", filename);
	
	document = xmlReadFile(filename, NULL, 0);
	root = xmlDocGetRootElement(document);
	printf("Root: <%s> (%i)\n", root->name, root->type);
	
	firstChild = root->children;
	node = firstChild;
	while (node->next) {
		node = node->next;
		
		printf("\tChild: <%s> (%i)\n", node->name, node->type);
		printf("\t\tContent: %s\n", node->content);
		
		if (node->children) {
			xmlNode *nestedNode = node->children;
			printf("\t\tChildren:\n");
			do {
				printf("\t\t\tChild: <%s> (%i)\n", nestedNode->name, nestedNode->type);
				nestedNode = nestedNode->next;
			} while (nestedNode);
		}
		
	}
	
	printf("\n\nEND OF RECORD\n");
	
    return 0;
}

